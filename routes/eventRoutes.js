'use strict';
const express = require('express');

const evenController = require('../controller/eventController');

const router = express.Router();

const {getEvents} = evenController;

router.get('/events',getEvents);

module.exports={
    routes : router
}

