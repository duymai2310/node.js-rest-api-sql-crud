'use strict'; 
const express = require('express'); 

var bodyParser = require('body-parser');

const eventRoutes = require('./routes/eventRoutes');

const config = require('./config');

const cors = require('cors'); 


const app = express(); 

app.use(cors());

app.use(bodyParser.json())



app.use('/api',eventRoutes.routes);

app.listen(config.port,()=> console.log(`server: http://localhost:${config.port}`) );